from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token


@receiver(post_save, sender=User)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


class ExtraInfo(models.Model):
    RODZAJE = {
        (0, 'Nieznany'),
        (1, 'Drama'),
        (2, 'Sci-FI'),
        (3, 'Mistic'),
        (4, 'Psychologiczne'),
        (5, 'Komedia')
    }

    czas_trwania = models.IntegerField(null=True, blank=True)
    rodzaj = models.IntegerField(choices=RODZAJE, default=0)

    # def __str__(self):
    #     return self.ekstra_info()
    #
    # def ekstra_info(self):
    #     return str(self.rodzaj)


class Film(models.Model):
    tytul = models.CharField(max_length=32)
    opis = models.TextField(max_length=1000)
    po_premierze = models.BooleanField(default=False)
    premiera = models.DateField(null=True, blank=True)
    rok = models.IntegerField(null=True, blank=True)
    imdb_rating = models.DecimalField(max_digits=4, decimal_places=2, null=True, blank=True)
    extra_info = models.OneToOneField(ExtraInfo,
                                      on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.nazwa_filmu()

    def nazwa_filmu(self):
        return self.tytul + " (" + str(self.rok) + ")"


class Recenzja(models.Model):
    opis = models.TextField(default='')
    gwiazdki = models.IntegerField(default=5)
    film = models.ForeignKey(Film, on_delete=models.CASCADE,
                             related_name="recenzje")

    def __str__(self):
        return self.recenzja_filmu()

    def recenzja_filmu(self):
        return str(self.film)


class Aktorzy(models.Model):
    imie = models.CharField(max_length=32)
    nazwisko = models.CharField(max_length=32)
    filmy = models.ManyToManyField(Film)
