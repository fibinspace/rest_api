from django.contrib.auth.models import User
from django.http.response import HttpResponseNotAllowed
from rest_framework import viewsets, filters
from rest_framework import permissions
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import action
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticated

from api.serializers import UserSerializer
from .models import Film, Recenzja, Aktorzy
from api.serializers import FilmSerializer, RecenzjaSerializer, AktorzySerializer, FilmMiniSerializer
from rest_framework.response import Response


class FilmViewSetPagination(PageNumberPagination):
    page_size = 2
    page_size_query_param = 'page_size'
    max_page_size = 3


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    # permission_classes = [permissions.IsAuthenticated]


class FilmViewSet(viewsets.ModelViewSet):
    queryset = Film.objects.all()
    serializer_class = FilmSerializer
    permission_classes = [permissions.IsAuthenticated]
    filter_backends = [filters.SearchFilter, filters.OrderingFilter]
    filter_fields = ['tytul', 'opis', 'rok']
    search_fields = ['tytul', 'opis']
    ordering_fields = '__all__'
    # ordering = ['-rok']
    pagination_class = FilmViewSetPagination
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        # rok = self.request.query_params.get('rok', None)
        # id = self.request.query_params.get('id', None)
        #
        # if id:
        #     filmy = Film.objects.filter(id=id)
        # else:
        #     if rok:
        #         filmy = Film.objects.filter(rok=rok)
        #     else:
        #         filmy = Film.objects.all()
        filmy = Film.objects.all()
        return filmy

    # def list(self, request, *args, **kwargs):
    #     # queryset = self.get_queryset()
    #     #
    #     # serializer = FilmSerializer(queryset, many=True)
    #
    #     tytul = self.request.query_params.get('tytul', None)
    #
    #     # filmy = Film.objects.filter(tytul__exact=tytul)
    #     # filmy = Film.objects.filter(tytul__contains=tytul)
    #     # filmy = Film.objects.filter(tytul__icontains=tytul)
    #
    #     filmy = Film.objects.filter(premiera__year="2021")
    #
    #     serializer = FilmSerializer(filmy, many=True)
    #     return Response(serializer.data)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = FilmSerializer(instance)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        # if request.user.is_superuser:
        film = Film.objects.create(tytul=request.data['tytul'],
                                   opis=request.data['opis'],
                                   po_premierze=request.data['po_premierze'],
                                   filmy=request.data['filmy'],
                                   rok=request.data['rok'])
        serializer = FilmSerializer(film, many=False)
        return Response(serializer.data)
        # else:
        # return HttpResponseNotAllowed('Not Allowed')

    def update(self, request, *args, **kwargs):
        film = self.get_object()
        film.tytul = request.data['tytul']
        film.opis = request.data['opis']
        film.po_premierze = request.data['po_premierze']
        film.filmy = request.data['filmy']
        film.rok = request.data['rok']
        film.save()

        serializer = FilmSerializer(film, many=False)
        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        film = self.get_object()
        film.delete()
        return Response('Film usunięty')

    @action(detail=True)
    def premiera(self, request, **kwargs):
        film = self.get_object()
        film.po_premierze = True
        film.save()

        serializer = FilmSerializer(film, many=False)
        return Response(serializer.data)

    @action(detail=False)
    def premiera_wszystkie(self, request, **kwargs):
        filmy = Film.objects.all()
        filmy.update(po_premierze=True)

        serializer = FilmSerializer(filmy, many=True)
        return Response(serializer.data)

    @action(detail=False, methods=['post'])
    def premiera_postman(self, request, **kwargs):
        filmy = Film.objects.all()
        filmy.update(po_premierze=request.data['po_premierze'])

        serializer = FilmSerializer(filmy, many=True)
        return Response(serializer.data)


class RecenzjaViewSet(viewsets.ModelViewSet):
    queryset = Recenzja.objects.all()
    serializer_class = RecenzjaSerializer


class AktorzyViewSet(viewsets.ModelViewSet):
    queryset = Aktorzy.objects.all()
    serializer_class = AktorzySerializer

    @action(detail=True, methods=['post'])
    def dolacz(self, request, **kwargs):
        aktor = self.get_object()
        film = Film.objects.get(id=request.data['film'])
        aktor.filmy.add(film)

        serializer = AktorzySerializer(aktor, many=False)
        return Response(serializer.data)
